-- Relational code

drop schema if exists relational cascade;

create schema relational;

set search_path = relational;

create table users (
	username	text
,	profile		text
,	full_name	text
,	email		text
,	created_at	date
,	addresses	jsonb
,	primary key (username,profile)
);

insert into users values ('alexdebrie', 'alexdebrie', 'Alex DeBrie', 'alexdebrie1@gmail.com', '2018-03-23', '{"Home": { "StreetAddress": "1111 1st St", "State": "Nebraska", "Country": "USA" }}');
insert into users values ('nedstark', 'nedstark', 'Eddard Stark', 'lord@winterfell.com', '2016-02-27', '{"Home": {"StreetAddress": "1234 2nd Ave", "City": "Winterfell", "Country": "Westeros"}, "Business": {"StreetAddress": "Suite 200, Red Keep", "City": "King''s Landing", "Country": "Westeros"}}');

create table orders (
	username	text
,	profile		text
,	order_id	text
,	status		text
,	placed_id	text
,	created_at	date
,	user_address	jsonb
,	primary key (order_id)
,	foreign key (username, profile) references users (username, profile)
,	constraint placed_id_valid check(
		status = 'PLACED' and placed_id is not null
		or
		status != 'PLACED' and placed_id is null
	)
);

insert into orders values ('alexdebrie', 'alexdebrie', '5e7272b7', 'PLACED', 'fb7f', '2019-04-21', '{ "StreetAddress": "1111 1st St", "State": "Nebraska", "Country": "USA" }');
insert into orders values ('alexdebrie', 'alexdebrie', '42ef295e', 'PLACED', 'c81c', '2019-04-25', '{ "StreetAddress": "1111 1st St", "State": "Nebraska", "Country": "USA" }');
insert into orders values ('alexdebrie', 'alexdebrie', '2e7abecc', 'SHIPPED', null, '2018-12-25', '{ "StreetAddress": "1111 1st St", "State": "Nebraska", "Country": "USA" }');
insert into orders values ('nedstark', 'nedstark', '2eae1dee', 'SHIPPED', null, '2019-01-15', '{"StreetAddress": "Suite 200, Red Keep", "City": "King''s Landing", "Country": "Westeros"}');
insert into orders values ('nedstark', 'nedstark', 'f4f80a91', 'PLACED', 'ac40', '2019-05-12', '{"StreetAddress": "Suite 200, Red Keep", "City": "King''s Landing", "Country": "Westeros"}');

create table order_items (
	item_id		text
,	order_id	text		references orders(order_id)
,	product_name	text	
,	price		numeric
,	status		text
,	quantity	int
,	primary key (item_id, order_id)
);

insert into order_items values ('ab070628', '5e7272b7', 'Amazon Echo', 59.99, 'FILLED');
insert into order_items values ('4cc734ec', '5e7272b7', 'Macbook Pro', 1399.99, 'FILLED');
insert into order_items values ('e16f4906', '2eae1dee', 'Duct tape', 5.99, 'FILLED');


-- Emulate single table design and GSIs with regular (consistent) views

create or replace view main_table as
	select	textcat('USER#', username) as pk,
		textcat('#PROFILE#', profile) as sk,
		row_to_json(users.*) as item
	from	users

	union all

	select	textcat('USER#', username),
		textcat('ORDER#', order_id),
		row_to_json(orders.*)
	from	orders

	union all

	select	textcat('ITEM#', item_id),
		textcat('ORDER#', order_id),
		row_to_json(order_items.*)
	from	order_items

	order by pk, sk
;

create index users_username_main_table on users (textcat('USER#', username));
create index orders_username_main_table on orders (textcat('USER#', username));
create index order_items_item_id_main_table on order_items (textcat('ITEM#', item_id));

create or replace view gs1 as
	select	sk as pk, pk as sk, item
	from	main_table
	order by 1, 2
;

create index users_profile_gs1 on users (textcat('#PROFILE#', profile));
create index orders_order_id_gs1 on orders (textcat('ORDER#', order_id));
create index order_items_order_id_gs1 on order_items (textcat('ORDER#', order_id));


create or replace view gs2 as
	select	textcat('USER#', username) as pk,
		textcat(status, textcat('#', created_at::text)) as sk,
		row_to_json(orders.*) as item
	from	orders
	order by pk, sk
;

create or replace view gsi_placedid as
	select	placed_id, username, order_id, status, created_at
	from	orders
	where	placed_id is not null
;


-- Emulate the queries running on the views (could be made simpler and more efficient if done over relational schema, obviously)

---- 1. Get user profile(s)
select * from main_table where pk = textcat('USER#', 'alexdebrie') and sk LIKE '#PROFILE#%';

---- 2. Get orders for user
select * from main_table where pk = textcat('USER#', 'alexdebrie') and sk LIKE 'ORDER#%';

---- 3. Get single order and order items
select * from gs1 where pk = textcat('ORDER#', '5e7272b7') and sk like 'ITEM#%';

---- 4. Get orders for users by status
select * from gs2 where pk = textcat('USER#', 'alexdebrie') and sk like 'SHIPPED#%';

---- 5. Get open orders
select * from gsi_placedid;
