# DynamoDB transactions collision study

This project contains a Java software project to study the potential collisions that may appear when running overlapping
write transactions in DynamoDB. Two transactions are considered to overlap if they cause modifications to at least one
item in common. According to DynamoDB's documentation, the second transaction to arrive will be rejected, and a manual
user retry is expected.

This project tests configurable contention scenarios and counts the rejections and the effective (user-perceived)
latency. To do so, it constantly updates a single item in several tables at once, as part of a single transaction.
Several of such updaters run concurrently.

You may specify how many tables to consider in total (up to the maximum of 25 operations within a DynamoDB transaction)
and how many tables are touched by any given transaction. You may also specify the sleep time between each updater
executing transactions, the concurrency level and the number of iterations --as well as the authentication information
to the DynamoDB database. See `dynamotxs.properties.sample` for a sample configuration file.


## Compiling the code

```sh
mvn package
```

The resulting jar file may be located in the `target/` folder with a name following a pattern similar to
`dynamotxs-1.0-SNAPSHOT-jar-with-dependencies.jar`.


## Running the code

Create a configuration file using `dynamotxs.properties.sample` as a starting point. This file must be the first
argument when running the program. The second is the mode of operations, which can be either of the following four:

1. `create_tables`. Creates the tables. The operation is asynchronous and may take some seconds to complete.
1. `load_items`. Inserts a single item per table, with a hash key and a numeric field which will be updated repeatedly.
1. `run`. Runs the simulation. The result is printed as a CSV in the standard output.
1. `delete_tables`. Cleanup.

To run the program use:

`java -jar target/dynamotxs-1.0-SNAPSHOT-jar-with-dependencies.jar config.properties operation`


## No responsibility

Running this code may cause you incur in AWS charges. The author(s) is(are) not responsible for any charges incurred. Do
not run on a production environment.
