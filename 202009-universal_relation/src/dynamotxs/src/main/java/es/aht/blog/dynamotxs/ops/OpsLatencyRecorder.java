package es.aht.blog.dynamotxs.ops;


import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;


// It would be much better to use here https://github.com/HdrHistogram/HdrHistogram, but KISS
public class OpsLatencyRecorder {
    private final AtomicInteger numberOperations = new AtomicInteger(0);
    private final AtomicLong aggregatedLatency = new AtomicLong(0L);    // Assuming it won't overflow

    public void recordOperationLatency(long millis) {
        numberOperations.incrementAndGet();
        aggregatedLatency.addAndGet(millis);
    }

    // Only safe to call if no recordOperationLatency() operation is in progress
    public int averageLatencyMillis() {
        return numberOperations.get() > 0 ? (int) aggregatedLatency.get() / numberOperations.get() : -1;
    }
}
