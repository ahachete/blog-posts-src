package es.aht.blog.dynamotxs.ops;


import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.*;
import es.aht.blog.dynamotxs.CommonUtils;
import es.aht.blog.dynamotxs.Configuration;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static es.aht.blog.dynamotxs.Configuration.SINGLE_ITEM_UPDATEABLE_FIELD;


public class Updater implements Callable<Void> {
    private static final String TRANSACTION_CONFLICT_CODE = "TransactionConflict";

    private final AmazonDynamoDB dynamoDB;
    private final int id;
    private final Configuration configuration;
    private final OpsLatencyRecorder opsLatencyRecorder;
    private final ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();

    private int failedTransactions = 0;

    public Updater(int id, Configuration configuration, OpsLatencyRecorder opsLatencyRecorder) {
        this.dynamoDB = CommonUtils.getDynamoDB(configuration);
        this.configuration = configuration;
        this.id = id;
        this.opsLatencyRecorder = opsLatencyRecorder;
    }

    @Override
    public Void call() throws Exception {
        int i = 0;
        while (i < configuration.getIterations()) {
            var start = LocalTime.now();
            var totalWaitTime = 0L;
            while(! updateTransaction()) {
                var waitTime = threadLocalRandom.nextLong(configuration.getMaxSleepBetweenCallsMillis());
                Thread.sleep(waitTime);
                totalWaitTime += waitTime;
                failedTransactions++;
            }
            var duration = Duration.between(start, LocalTime.now()).toMillis();
            var effectiveDuration = duration - totalWaitTime;
            opsLatencyRecorder.recordOperationLatency(effectiveDuration);
            i++;
        }

        return null;
    }

    public int getFailedTransactions() {
        return failedTransactions;
    }

    private boolean updateTransaction() {
        var tables = CommonUtils.allTablesStream(configuration).collect(Collectors.toList());

        // Select tablesPerTransaction tables, randomized. Shuffle the previous stream and limit it
        Collections.shuffle(tables);

        var transactWriteItems = tables.stream()
                .limit(configuration.getTablesPerTransaction())
                .map(tableName -> generateUpdate(tableName, id))
                .collect(Collectors.toList());

        TransactWriteItemsRequest transactWriteItemsRequest = new TransactWriteItemsRequest();
        transactWriteItemsRequest.setTransactItems(transactWriteItems);

        try {
            dynamoDB.transactWriteItems(transactWriteItemsRequest);
        } catch (TransactionCanceledException exception) {
            boolean transactionConflict = exception.getCancellationReasons().stream()
                    .filter(p -> TRANSACTION_CONFLICT_CODE.equals(p.getCode()))
                    .count() > 0L;
            if(transactionConflict) {
                return false;
            } else {
                // Unexpected exception, abort
                throw new RuntimeException(exception);
            }
        }

        return true;
    }

    private TransactWriteItem generateUpdate(String tableName, int increment) {
        Update update = new Update();
        update.setTableName(tableName);
        update.setKey(CommonUtils.getItemKey());
        update.setUpdateExpression("SET " + SINGLE_ITEM_UPDATEABLE_FIELD + " = " + SINGLE_ITEM_UPDATEABLE_FIELD + " + :inc");
        update.setExpressionAttributeValues(Map.of(":inc", new AttributeValue().withN(Integer.toString(increment))));

        var transactWriteItem = new TransactWriteItem();
        transactWriteItem.setUpdate(update);

        return transactWriteItem;
    }
}
