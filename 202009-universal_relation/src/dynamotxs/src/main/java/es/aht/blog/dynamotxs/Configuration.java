package es.aht.blog.dynamotxs;


import com.amazonaws.regions.Regions;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.stream.IntStream;


public class Configuration {
    public static final String TABLE_PK_NAME = "pk";
    public static final String SINGLE_ITEM_PK_VALUE = "thevalue";
    public static final String SINGLE_ITEM_UPDATEABLE_FIELD = "updateme";
    public static final int DYNAMODB_TX_MAX_NUMBER_TABLES = 25;
    
    private final Regions awsRegion;
    private final String awsProfile;
    private final int numberTables;
    private final int tablesPerTransaction;
    private final int iterations;
    private final int maxThreads;
    private final long maxSleepBetweenCallsMillis;

    public Configuration(
            Regions awsRegion, String awsProfile, int numberTables, int tablesPerTransaction, int iterations,
            int maxThreads, long maxSleepBetweenCallsMillis
    ) {
        this.awsRegion = awsRegion;
        this.awsProfile = awsProfile;
        this.numberTables = numberTables;
        this.tablesPerTransaction = tablesPerTransaction;
        this.iterations = iterations;
        this.maxThreads = maxThreads;
        this.maxSleepBetweenCallsMillis = maxSleepBetweenCallsMillis;
    }

    public Regions getAwsRegion() {
        return awsRegion;
    }

    public String getAwsProfile() {
        return awsProfile;
    }

    public int getNumberTables() {
        return numberTables;
    }

    public int getTablesPerTransaction() {
        return tablesPerTransaction;
    }

    public int getIterations() {
        return iterations;
    }

    public int getMaxThreads() {
        return maxThreads;
    }

    public IntStream pow2IncreasingNumberThreads() {
        return IntStream.rangeClosed(0, Integer.numberOfTrailingZeros(maxThreads))
                .map(i -> 1 << i);
    }

    public long getMaxSleepBetweenCallsMillis() {
        return maxSleepBetweenCallsMillis;
    }

    public static class InvalidConfigurationException extends Exception {
        public InvalidConfigurationException(String message) {
            super(message);
        }
    }
    
    public static class PropertiesLoader {
        private static final String PROPERTIES_AWS_REGION = "awsRegion";
        private static final String PROPERTIES_AWS_PROFILE = "awsProfile";
        private static final String PROPERTIES_NUMBER_TABLES = "numberTables";
        private static final String PROPERTIES_TABLES_PER_TRANSACTION = "tablesPerTransaction";
        private static final String PROPERTIES_ITERATIONS = "iterations";
        private static final String PROPERTIES_MAX_THREADS = "maxThreads";
        private static final String PROPERTIES_SLEEP_BETWEEN_CALLS_MILLIS = "maxSleepBetweenCallsMillis";

        private static final int DEFAULT_NUMBER_TABLES = 25;
        private static final int DEFAULT_TABLES_PER_TRANSACTION = 2;
        private static final int DEFAULT_ITERATIONS = 10;
        private static final int DEFAULT_MAX_THREADS = 2;
        private static final int DEFAULT_SLEEP_BETWEEN_CALLS_MILLIS = 2_000;

        private Regions awsRegion;
        private String awsProfile;
        private int numberTables;
        private int tablesPerTransaction;
        private int iterations;
        private int maxThreads;
        private int sleepBetweenCallsMillis;

        public Configuration load(String file) throws InvalidConfigurationException, IOException {
            var properties = new Properties();
            try(var reader = new FileReader(file)) {
                properties.load(reader);
            } catch (FileNotFoundException exception) {
                throw new InvalidConfigurationException("Configuration file not found");
            }

            if(! (properties.containsKey(PROPERTIES_AWS_REGION) && properties.containsKey(PROPERTIES_AWS_PROFILE))) {
                throw new InvalidConfigurationException("Configuration file does not include AWS configuration");
            }

            try {
                awsRegion = Regions.fromName(properties.getProperty(PROPERTIES_AWS_REGION));
            } catch (IllegalArgumentException exception) {
                throw new InvalidConfigurationException(
                        "Invalid AWS Region " + properties.getProperty(PROPERTIES_AWS_REGION)
                );
            }
            awsProfile = properties.getProperty(PROPERTIES_AWS_PROFILE);
            numberTables = getIntPropertyOrElse(properties, PROPERTIES_NUMBER_TABLES, DEFAULT_NUMBER_TABLES);
            if(numberTables > DYNAMODB_TX_MAX_NUMBER_TABLES) {
                throw new InvalidConfigurationException(
                        "Number of tables must be <= " + DYNAMODB_TX_MAX_NUMBER_TABLES
                );
            }
            tablesPerTransaction = getIntPropertyOrElse(
                    properties, PROPERTIES_TABLES_PER_TRANSACTION, DEFAULT_TABLES_PER_TRANSACTION
            );
            if(tablesPerTransaction > numberTables) {
                throw new InvalidConfigurationException(
                        "Number of tables per transaction needs to be equal to or less than numberTables"
                );
            }
            iterations = getIntPropertyOrElse(properties, PROPERTIES_ITERATIONS, DEFAULT_ITERATIONS);
            maxThreads = getIntPropertyOrElse(properties, PROPERTIES_MAX_THREADS, DEFAULT_MAX_THREADS);
            var pow2Threads = Integer.highestOneBit(maxThreads);
            sleepBetweenCallsMillis = getIntPropertyOrElse(
                    properties, PROPERTIES_SLEEP_BETWEEN_CALLS_MILLIS, DEFAULT_SLEEP_BETWEEN_CALLS_MILLIS
            );

            return new Configuration(
                    awsRegion, awsProfile, numberTables, tablesPerTransaction, iterations, pow2Threads,
                    sleepBetweenCallsMillis
            );
        }

        private int getIntPropertyOrElse(Properties properties, String propertyName, int elseValue)
        throws InvalidConfigurationException {
            if(! properties.containsKey(propertyName)) {
                return elseValue;
            }

            try {
                return Integer.parseInt(properties.getProperty(propertyName));
            } catch (NumberFormatException exception) {
                throw new InvalidConfigurationException("Configuration parameter must be a number: " + propertyName);
            }
        }
    }
}
