package es.aht.blog.dynamotxs.ops;


import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import es.aht.blog.dynamotxs.CommonUtils;
import es.aht.blog.dynamotxs.Configuration;

import static es.aht.blog.dynamotxs.Configuration.SINGLE_ITEM_UPDATEABLE_FIELD;


public class LoadItems extends AbstractOperation {
    public LoadItems(Configuration configuration) {
        super(configuration);
    }

    public void run() {
        var dynamoDB = CommonUtils.getDynamoDB(configuration);

        var updateableFieldVale = new AttributeValue();
        updateableFieldVale.setN("0");
        var item = CommonUtils.getItemKey();
        item.put(SINGLE_ITEM_UPDATEABLE_FIELD, updateableFieldVale);
        CommonUtils.allTablesStream(configuration).forEach(
                tableName -> dynamoDB.putItem(
                        new PutItemRequest(tableName, item)
                )
        );
    }
}
