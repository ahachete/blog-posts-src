package es.aht.blog.dynamotxs.ops;


import es.aht.blog.dynamotxs.CommonUtils;
import es.aht.blog.dynamotxs.Configuration;


public class DeleteTables extends AbstractOperation {
    public DeleteTables(Configuration configuration) {
        super(configuration);
    }

    public void run() {
        var dynamoDB = CommonUtils.getDynamoDB(configuration);

        CommonUtils.allTablesStream(configuration).forEach(
                tableName -> dynamoDB.deleteTable(tableName)
        );
    }
}
