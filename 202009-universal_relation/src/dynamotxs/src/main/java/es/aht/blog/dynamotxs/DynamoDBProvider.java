package es.aht.blog.dynamotxs;


import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;


public class DynamoDBProvider {
    public static AmazonDynamoDB getDynamoDB(Configuration configuration) {
        return AmazonDynamoDBClientBuilder.standard()
                .withRegion(configuration.getAwsRegion())
                .withClientConfiguration(
                        new ClientConfiguration().withTcpKeepAlive(true)
                )
                .withCredentials(new ProfileCredentialsProvider(configuration.getAwsProfile()))
                .build();
    }
}
