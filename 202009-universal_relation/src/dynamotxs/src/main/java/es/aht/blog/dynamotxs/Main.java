package es.aht.blog.dynamotxs;


import es.aht.blog.dynamotxs.ops.CreateTables;
import es.aht.blog.dynamotxs.ops.DeleteTables;
import es.aht.blog.dynamotxs.ops.LoadItems;
import es.aht.blog.dynamotxs.ops.TxUpdates;

import java.io.IOException;


public class Main {
    public enum Operations { create_tables, load_items, delete_tables, run; }

    private static void usageAndExit(String message) {
        System.err.println(message);
        System.err.println("Usage: java -jar ... configuration.properties (create_tables | delete_tables | run)");
        System.exit(1);
    }

    public static void main(String[] args) throws IOException {
        if(args == null || args.length != 2) {
            usageAndExit("Invalid number of arguments");
        }

        Configuration configuration = null;
        try {
            configuration = new Configuration.PropertiesLoader().load(args[0]);
        } catch (Configuration.InvalidConfigurationException exception) {
            usageAndExit(exception.getMessage());
        }

        switch (Operations.valueOf(args[1])) {
            case create_tables:
                new CreateTables(configuration).run();
                break;
            case load_items:
                new LoadItems(configuration).run();
                break;
            case delete_tables:
                new DeleteTables(configuration).run();
                break;
            case run:
                System.out.println(
                        new TxUpdates(configuration).run()
                );
                break;
            default:
                usageAndExit("Invalid operation");
        }
    }
}
