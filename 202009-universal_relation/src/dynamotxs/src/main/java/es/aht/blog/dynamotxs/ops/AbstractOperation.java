package es.aht.blog.dynamotxs.ops;


import es.aht.blog.dynamotxs.Configuration;


public abstract class AbstractOperation {
    protected Configuration configuration;

    public AbstractOperation(Configuration configuration) {
        this.configuration = configuration;
    }
}
