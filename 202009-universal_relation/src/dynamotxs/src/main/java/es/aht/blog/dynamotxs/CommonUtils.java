package es.aht.blog.dynamotxs;


import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static es.aht.blog.dynamotxs.Configuration.*;


public class CommonUtils {
    private static final String TABLE_NAME_PREFIX = "tbl";

    public static AmazonDynamoDB getDynamoDB(Configuration configuration) {
        return DynamoDBProvider.getDynamoDB(configuration);
    }

    public static Stream<String> allTablesStream(Configuration configuration) {
        return IntStream.range(0, configuration.getNumberTables())
                .mapToObj(i -> TABLE_NAME_PREFIX + (i + 1));
    }

    public static Map<String, AttributeValue> getItemKey() {
        Map<String, AttributeValue> result = new HashMap<>();
        result.put(
                TABLE_PK_NAME, new AttributeValue(SINGLE_ITEM_PK_VALUE)
        );

        return result;
    }
}
