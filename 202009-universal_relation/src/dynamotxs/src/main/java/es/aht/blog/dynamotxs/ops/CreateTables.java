package es.aht.blog.dynamotxs.ops;


import com.amazonaws.services.dynamodbv2.model.*;
import es.aht.blog.dynamotxs.CommonUtils;
import es.aht.blog.dynamotxs.Configuration;

import java.util.Arrays;

import static es.aht.blog.dynamotxs.Configuration.TABLE_PK_NAME;


public class CreateTables extends AbstractOperation {
    public CreateTables(Configuration configuration) {
        super(configuration);
    }

    public void run() {
        var dynamoDB = CommonUtils.getDynamoDB(configuration);

        CommonUtils.allTablesStream(configuration).forEach(
                tableName -> dynamoDB.createTable(genCreateTableRequest(tableName))
        );
    }

    private CreateTableRequest genCreateTableRequest(String tableName) {
        var createTableRequest = new CreateTableRequest();

        createTableRequest.setTableName(tableName);
        createTableRequest.setKeySchema(
                Arrays.asList(new KeySchemaElement(TABLE_PK_NAME, KeyType.HASH))
        );
        createTableRequest.setAttributeDefinitions(
                Arrays.asList(new AttributeDefinition(TABLE_PK_NAME, ScalarAttributeType.S))
        );
        createTableRequest.setBillingMode("PAY_PER_REQUEST");

        return createTableRequest;
    }
}
