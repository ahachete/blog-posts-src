package es.aht.blog.dynamotxs.ops;


import es.aht.blog.dynamotxs.Configuration;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;


public class TxUpdates extends AbstractOperation {
    private static final int THREADS_DURING_WARMUP = 4;


    public TxUpdates(Configuration configuration) {
        super(configuration);
    }

    public String run() {
        var stringBuilder = new StringBuilder();
        stringBuilder
                .append("tables: ").append(configuration.getNumberTables()).append("\t")
                .append("iterations: ").append(configuration.getIterations()).append("\t")
                .append("max_sleep: ").append(configuration.getMaxSleepBetweenCallsMillis()).append("\t")
                .append("\n")
                .append("threads").append("\t")
                .append("pct_retries").append("\t")
                .append("avg_latency").append("\t")
        ;

        // warmup, ignore results
        run(THREADS_DURING_WARMUP, null, true);

        // real runs
        configuration.pow2IncreasingNumberThreads()
                .forEach(threads -> run(threads, stringBuilder, false));

        return stringBuilder.toString();
    }

    private void run(int threads, StringBuilder stringBuilder, boolean warmup) {
        var totalOperations = configuration.getIterations() * configuration.getMaxThreads();
        var opsLatencyRecorder = new OpsLatencyRecorder();
        var totalRetries = runUpdates(opsLatencyRecorder, threads);

        if(warmup) {
            return;
        }

        stringBuilder
                .append("\n")
                .append(threads).append("\t")
                .append(Math.round(totalRetries * 100f / totalOperations)).append("\t\t")
                .append(opsLatencyRecorder.averageLatencyMillis())
        ;
    }

    private int runUpdates(OpsLatencyRecorder opsLatencyRecorder, int threads) {
        var executorService = Executors.newFixedThreadPool(threads);

        var updaters = new ArrayList<Updater>();
        IntStream.rangeClosed(1, configuration.getMaxThreads()).forEach(i ->
                updaters.add(new Updater(i, configuration, opsLatencyRecorder))
        );

        try {
            executorService.invokeAll(updaters);
            executorService.shutdown();
            executorService.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException exception) {
            Thread.currentThread().interrupt();
        }

        return updaters.stream()
                .map(updater -> updater.getFailedTransactions())
                .reduce(0, Integer::sum);
    }
}
