#!/bin/sh


export REGION=us-east-2

aws="aws --region $REGION"


# Create UR table
$aws dynamodb create-table --table-name singletable --billing-mode PAY_PER_REQUEST \
	--attribute-definitions '[{"AttributeName": "PK", "AttributeType": "S"}, {"AttributeName": "SK", "AttributeType": "S"}]' \
	--key-schema '[{"AttributeName": "PK", "KeyType": "HASH"}, {"AttributeName": "SK", "KeyType": "RANGE"}]'


# Wait until table is created. Ideally, this should be a while loop calling describe-table and wait until table is created. KISS here, simple sleep
sleep 1m


# Insert table data
$aws dynamodb put-item --table-name singletable --item '{"PK": {"S":"ITEM#4cc734ec"}, "SK": {"S": "ORDER#5e7272b7"}, "ItemId": {"S": "4cc734ec"}, "OrderId": {"S": "5e7272b7"}, "ProductName": {"S": "Macbook Pro"}, "Price": {"N": "1399.99"}, "Status": {"S": "FILLED"}}'
$aws dynamodb put-item --table-name singletable --item '{"PK": {"S":"ITEM#ab070628"}, "SK": {"S": "ORDER#5e7272b7"}, "ItemId": {"S": "ab070628"}, "OrderId": {"S": "5e7272b7"}, "ProductName": {"S": "Amazon Echo"}, "Price": {"N": "59.99"}, "Status": {"S": "FILLED"}}'
$aws dynamodb put-item --table-name singletable --item '{"PK": {"S":"ITEM#e16f4906"}, "SK": {"S": "ORDER#2eae1dee"}, "ItemId": {"S": "e16f4906"}, "OrderId": {"S": "2eae1dee"}, "ProductName": {"S": "Duct tape"}, "Price": {"N": "5.99"}, "Status": {"S": "FILLED"}}'
$aws dynamodb put-item --table-name singletable --item '{"PK": {"S":"USER#alexdebrie"}, "SK": {"S": "ORDER#2e7abecc"}, "Username": {"S": "alexdebrie"}, "Profile": {"S": "alexdebrie"}, "OrderId": {"S": "2e7abecc"}, "Status": {"S": "SHIPPED"}, "CreatedAt": {"S": "2018-12-25"}, "OrderStatusDate": {"S": "SHIPPED#2018-12-25"}, "UserAddress": {"M": {"State": {"S": "Nebraska"}, "Country": {"S": "USA"}, "StreetAddress": {"S": "1111 1st St"}}}}'
$aws dynamodb put-item --table-name singletable --item '{"PK": {"S":"USER#alexdebrie"}, "SK": {"S": "ORDER#42ef295e"}, "Username": {"S": "alexdebrie"}, "Profile": {"S": "alexdebrie"}, "OrderId": {"S": "42ef295e"}, "Status": {"S": "PLACED"}, "PlacedId": {"S": "c81c"}, "CreatedAt": {"S": "2019-04-25"}, "OrderStatusDate": {"S": "PLACED#2019-04-25"}, "UserAddress": {"M": {"State": {"S": "Nebraska"}, "Country": {"S": "USA"}, "StreetAddress": {"S": "1111 1st St"}}}}'
$aws dynamodb put-item --table-name singletable --item '{"PK": {"S":"USER#alexdebrie"}, "SK": {"S": "ORDER#5e7272b7"}, "Username": {"S": "alexdebrie"}, "Profile": {"S": "alexdebrie"}, "OrderId": {"S": "5e7272b7"}, "Status": {"S": "PLACED"}, "PlacedId": {"S": "fb7f"}, "CreatedAt": {"S": "2019-04-21"}, "OrderStatusDate": {"S": "PLACED#2019-04-21"}, "UserAddress": {"M": {"State": {"S": "Nebraska"}, "Country": {"S": "USA"}, "StreetAddress": {"S": "1111 1st St"}}}}'
$aws dynamodb put-item --table-name singletable --item '{"PK": {"S":"USER#alexdebrie"}, "SK": {"S": "#PROFILE#alexdebrie"}, "Username": {"S": "alexdebrie"}, "Profile": {"S": "alexdebrie"}, "FullName": {"S": "Alex DeBrie"}, "email": {"S": "alexdebrie1@gmail.com"}, "CreatedAt": {"S": "2018-03-23"}, "Addresses": {"L": [ {"M": {"Home": {"M": {"State": {"S": "Nebraska"}, "Country": {"S": "USA"}, "StreetAddress": {"S": "1111 1st St"}}}}} ]}}'
$aws dynamodb put-item --table-name singletable --item '{"PK": {"S":"USER#nedstark"}, "SK": {"S": "ORDER#2eae1dee"}, "Username": {"S": "nedstark"}, "Profile": {"S": "nedstark"}, "OrderId": {"S": "2eae1dee"}, "Status": {"S": "SHIPPED"}, "CreatedAt": {"S": "2019-01-15"}, "OrderStatusDate": {"S": "SHIPPED#2019-01-15"}, "UserAddress": {"M": {"City": {"S": "King''s Landing"}, "Country": {"S": "Westeros"}, "StreetAddress": {"S": "Suite 200, Red Keep"}}}}'
$aws dynamodb put-item --table-name singletable --item '{"PK": {"S":"USER#nedstark"}, "SK": {"S": "ORDER#f4f80a91"}, "Username": {"S": "nedstark"}, "Profile": {"S": "nedstark"}, "OrderId": {"S": "f4f80a91"}, "Status": {"S": "PLACED"}, "PlacedId": {"S": "ac40"}, "CreatedAt": {"S": "2019-05-12"}, "OrderStatusDate": {"S": "PLACED#2019-05-12"}, "UserAddress": {"M": {"City": {"S": "King''s Landing"}, "Country": {"S": "Westeros"}, "StreetAddress": {"S": "Suite 200, Red Keep"}}}}'
$aws dynamodb put-item --table-name singletable --item '{"PK": {"S":"USER#nedstark"}, "SK": {"S": "#PROFILE#nedstark"}, "Username": {"S": "nedstark"}, "Profile": {"S": "nedstark"}, "FullName": {"S": "Eddard Stark"}, "email": {"S": "lord@winterfell.com"}, "CreatedAt": {"S": "2016-02-27"}, "Addresses": {"L": [ {"M": {"Home": {"M": {"City": {"S": "Winterfell"}, "Country": {"S": "Westeros"}, "StreetAddress": {"S": "1234 2nd Ave"}}}}}, {"M": {"Business": {"M": {"City": {"S": "King''s Landing"}, "Country": {"S": "Westeros"}, "StreetAddress": {"S": "Suite 200, Red Keep"}}}}} ]}}'


# Create GSIs

# GS1
$aws dynamodb update-table --table-name singletable \
	--attribute-definitions '[{"AttributeName": "PK", "AttributeType": "S"}, {"AttributeName": "SK", "AttributeType": "S"}]' \
	--global-secondary-index-updates '[{"Create": {"IndexName": "GS1", "KeySchema": [{"AttributeName": "SK", "KeyType": "HASH"}, {"AttributeName": "PK", "KeyType": "RANGE"}], "Projection": {"ProjectionType": "ALL"}}}]'

# Wait until index is created
sleep 5m

# GS2
$aws dynamodb update-table --table-name singletable \
	--attribute-definitions '[{"AttributeName": "PK", "AttributeType": "S"}, {"AttributeName": "OrderStatusDate", "AttributeType": "S"}]' \
	--global-secondary-index-updates '[{"Create": {"IndexName": "GS2", "KeySchema": [{"AttributeName": "PK", "KeyType": "HASH"}, {"AttributeName": "OrderStatusDate", "KeyType": "RANGE"}], "Projection": {"ProjectionType": "INCLUDE", "NonKeyAttributes": ["Username", "OrderId", "Status", "CreatedAt"] }}}]'


# Wait until index is created
sleep 5m

# GSPlacedId
$aws dynamodb update-table --table-name singletable \
	--attribute-definitions '[{"AttributeName": "PlacedId", "AttributeType": "S"}]' \
	--global-secondary-index-updates '[{"Create": {"IndexName": "GSPlacedId", "KeySchema": [{"AttributeName": "PlacedId", "KeyType": "HASH"}], "Projection": {"ProjectionType": "INCLUDE", "NonKeyAttributes": ["Username", "OrderId", "Status", "CreatedAt"] }}}]'

# Wait until index is created
sleep 5m


# Queries

# 1. Get user profile(s)
$aws dynamodb query --table-name singletable --select ALL_ATTRIBUTES \
	--key-condition-expression 'PK = :user and begins_with(SK, :profile_prefix)' --expression-attribute-values '{":user": {"S": "USER#alexdebrie"}, ":profile_prefix": {"S": "#PROFILE#"}}'  

# 2. Get orders for user
$aws dynamodb query --table-name singletable --select ALL_ATTRIBUTES \
	--key-condition-expression 'PK = :user and begins_with(SK, :order_prefix)' --expression-attribute-values '{":user": {"S": "USER#alexdebrie"}, ":order_prefix": {"S": "ORDER#"}}' 

# 3. Get single order and order items
$aws dynamodb query --table-name singletable --index-name GS1 --select ALL_ATTRIBUTES \
	--key-condition-expression 'SK = :order and begins_with(PK, :item_prefix)' --expression-attribute-values '{":order": {"S": "ORDER#5e7272b7"}, ":item_prefix": {"S": "ITEM#"}}'

# 4. Get orders for users by status
$aws dynamodb query --table-name singletable --index-name GS2 --select ALL_PROJECTED_ATTRIBUTES \
	--key-condition-expression 'PK = :user and begins_with(OrderStatusDate, :shipped_prefix)' --expression-attribute-values '{":user": {"S": "USER#alexdebrie"}, ":shipped_prefix": {"S": "SHIPPED#"}}'

# 5. Get open orders
$aws dynamodb scan --table-name singletable --index-name GSPlacedId --select ALL_PROJECTED_ATTRIBUTES


# Delete table
$aws dynamodb delete-table --table-name singletable
